package pl.kasprowski.journalerapi.repositories

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import pl.kasprowski.journalerapi.model.Note

@Repository
interface NoteRepository : CrudRepository<Note, String>