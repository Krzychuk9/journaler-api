package pl.kasprowski.journalerapi.repositories

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import pl.kasprowski.journalerapi.security.User

@Repository
interface UserRepository : CrudRepository<User, String> {
    fun findUserByEmail(email: String): User?
}