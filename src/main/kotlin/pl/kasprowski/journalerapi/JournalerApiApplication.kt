package pl.kasprowski.journalerapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JournalerApiApplication

fun main(args: Array<String>) {
    runApplication<JournalerApiApplication>(*args)
}
