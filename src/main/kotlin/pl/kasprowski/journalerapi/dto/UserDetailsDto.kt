package pl.kasprowski.journalerapi.dto

import java.util.*

data class UserDetailsDto(val id: String,
                          var email: String,
                          var firstName: String,
                          var lastName: String,
                          var roles: String,
                          var enabled: Boolean,
                          var accountNotExpired: Boolean,
                          var accountNotLocked: Boolean,
                          var credentialsNotExpired: Boolean,
                          var created: Date,
                          var Modified: Date)