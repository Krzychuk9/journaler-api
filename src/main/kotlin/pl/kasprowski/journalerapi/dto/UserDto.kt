package pl.kasprowski.journalerapi.dto

data class UserDto(var email: String,
                   var password: String,
                   var firstName: String,
                   var lastName: String)