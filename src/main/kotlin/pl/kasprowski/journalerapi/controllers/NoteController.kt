package pl.kasprowski.journalerapi.controllers

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import pl.kasprowski.journalerapi.model.Note
import pl.kasprowski.journalerapi.services.NoteService

@RestController
@RequestMapping("/notes")
class NoteController(private val service: NoteService) {

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getNotes(): List<Note> = service.get()

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createNote(@RequestBody note: Note): Note = service.create(note)

    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateNote(@RequestBody note: Note): Note = service.update(note)

    @DeleteMapping("/{id}")
    fun deleteNote(@PathVariable id: String) = service.delete(id)
}