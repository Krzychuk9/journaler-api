package pl.kasprowski.journalerapi.security

import java.util.*
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity

@Entity
@DiscriminatorValue("MEMBER")
class Member(id: String,
             email: String,
             pwd: String,
             firstName: String,
             lastName: String,
             roles: String,
             enabled: Boolean,
             accountNotExpired: Boolean,
             accountNotLocked: Boolean,
             credentialsNotExpired: Boolean,
             created: Date,
             modified: Date)
    : User(id, email, pwd, firstName, lastName, roles, enabled, accountNotExpired, accountNotLocked, credentialsNotExpired, created, modified) {
    constructor() : this("", "", "", "", "", "", true, true, true, true, Date(), Date())
}