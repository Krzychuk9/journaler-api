package pl.kasprowski.journalerapi.services

import org.springframework.stereotype.Service
import pl.kasprowski.journalerapi.model.Note
import pl.kasprowski.journalerapi.repositories.NoteRepository

@Service
class NoteService(private val repository: NoteRepository) {
    fun get(): List<Note> = repository.findAll().toList()
    fun create(note: Note): Note = repository.save(note)
    fun update(note: Note): Note = repository.save(note)
    fun delete(id: String): Unit = repository.deleteById(id)
}